package ua.danit.fs;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Numbers {
    public static void main(String[]args){


        Random random = new Random();
        int randomNum = random.nextInt((100) + 1);

        Scanner in = new Scanner(System.in);

        System.out.println("Please enter your name!");
        String name = in.nextLine();

        int[] nums = new int[101];

        int num = -1;

        System.out.println("Let the game begin!");

        while(randomNum != num) {

            System.out.println("Please enter your number!");

            String str = in.next();
            try {
                num = Integer.valueOf(str);
            } catch (NumberFormatException e) {
                System.err.println("Not a number!");
                continue;
            }

            if (num < randomNum){
                System.out.println("Your number is too small. Please, try again.");
            } else if (num > randomNum){
                System.out.println("Your number is too big. Please, try again.");
            } else {
                System.out.println("Congratulations, " + name + "!");
                boolean isSorted = false;
                int buf;
                while(!isSorted) {
                    isSorted = true;
                    for (int i = 0; i < nums.length-1; i++) {
                        if(nums[i] < nums[i+1]){
                            isSorted = false;

                            buf = nums[i];
                            nums[i] = nums[i+1];
                            nums[i+1] = buf;
                        }
                    }
                }
                System.out.println("Your numbers: " + Arrays.toString(nums) + "!");
            }
            nums[num] = num;
            }
        }
    }